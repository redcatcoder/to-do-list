import React from 'react';
import ReactDOM from 'react-dom';

import './config/default.css';
import AppContainer from "./components/app-container";

const App = AppContainer;

ReactDOM.render( <App />, document.getElementById('root') );
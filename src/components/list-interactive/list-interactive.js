import React from "react";
import ListFilter from "../list-filter";

import './list-interactive.css';

const ListInteractive = ({onTypeSearch, onChangeFilter}) => {
	return (
		<section className="list list-interactive">
			<input className="list-interactive__search-panel" placeholder='Type here something'
				   onChange={ ( event ) => { onTypeSearch( event.target.value ) } } />
			<ListFilter className={"list-interactive__filter-buttons"} onChangeFilter={onChangeFilter} />
		</section>
	);
}

export default ListInteractive;
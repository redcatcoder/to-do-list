import React, {Component} from "react";
import AppButton from "../app-button";

import Config from "../../config/config";

import './list-item.css';

export default class ListItem extends Component {
	render() {
		const { label, done, important, hidden, onDeleted, onToggleImportant, onToggleDone } = this.props;

		let listItemClasses = "list-item element-interact";

		if (done) listItemClasses += " done";

		if (important) listItemClasses += " important";

		if (hidden) listItemClasses += " hidden";

		return (
			<li className={listItemClasses} onClick={ onToggleDone }>
				<span className="list-item__name" >{ label }</span>
				<AppButton icon={Config.icons.trash} text={"Delete the item"} selectors={["btn-list-item", "btn-trash"]} onClick={ (event) => { event.stopPropagation(); onDeleted() } }/>
				<AppButton icon={Config.icons.important} text={"Make the item an important"} selectors={["btn-list-item", "btn-important"]} onClick={ (event) => { event.stopPropagation(); onToggleImportant() }  }/>
			</li>
		);
	}
}
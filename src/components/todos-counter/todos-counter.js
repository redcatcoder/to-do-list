import React from 'react';

import './todos-counter.css';

const ToDosCounter = ({className, done, left}) => {
	return <span className={className}>{done} done, {left} left</span>;
}

export default ToDosCounter;
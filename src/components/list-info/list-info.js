import React from "react";
import ToDosCounter from "../todos-counter";

import './list-info.css';

const ListInfo = ({done = 0, left = 0}) => {
	return (
		<section className="list list-info">
			<h1 className="list-info__title">My ToDo List</h1>
			<ToDosCounter className={"list-info__todos-count"} done={done} left={left}/>
		</section>
	);
}

export default ListInfo;
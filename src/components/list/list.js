import React from "react";
import ListItem from "../list-item/";

import './list.css';

const List = ({ todos, onDeleted, onToggleImportant, onToggleDone }) => {

	const elements = todos.map( (item, index) => {
		return <ListItem
			key={index}
			{...item}
			onDeleted={() => onDeleted(index)}
			onToggleImportant={() => onToggleImportant(index)}
			onToggleDone={() => onToggleDone(index)}
		/>;
	})

	return (
		<section className="list list-content">
			<div className="list-animate-background">
				<ul className="list-content__group">
					{ elements }
				</ul>
			</div>

		</section>
	);
}

export default List;
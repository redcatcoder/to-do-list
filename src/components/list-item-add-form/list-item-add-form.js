import React, {Component} from 'react';
import AppButton from "../app-button";

import './list-item-add-form.css';

export default class ListItemAddForm extends Component {

	onLabelChange = ( event ) => {
		this.setState({
			label: event.target.value
		});
	};

	onSubmit = ( event ) => {
		event.preventDefault();
		this.props.onItemAdded( this.state.label )
		this.setState({
			label: ''
		});
	}

	state = {
		label: ''
	};

	render() {
		return (
			<form className="list-item-add-form" onSubmit={this.onSubmit}>
				<AppButton type={'submit'} selectors={['list-item-add-form__btn', 'btn-add-new-list-item']}
						   text={'+'} title={"Click to add a new item into list"} />
				<input type="text" className="list-item-add-form__input"
					   placeholder="What needs to be done?" onChange={this.onLabelChange} value={this.state.label} />
			</form>
		)
	}
}

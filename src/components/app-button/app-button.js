import React, {useEffect} from "react";

const AppButton = ({type = 'button', text = 'Button', selectors = [], icon = undefined, title = text, data = {}, onClick = undefined}) => {
	const classList = ['btn', ...selectors].toString().replaceAll(',', ' ');
	const output = (icon !== undefined) ? React.createElement('img', {src: icon, alt: text, className: 'icon'} ): text;

	const btnId = `${Math.random()}`.replace('.', '');

	const inputData = Object.entries(data);

	useEffect(() => {
		if ( inputData.length > 0 ) {
			for ( const [key, value] of inputData ) {
				document.getElementById(btnId).setAttribute(`data-${key}`, `${value}`);
			}
		}
	});

	return (
		<button id={btnId} type={type} className={classList !== '' ? classList : undefined} title={title} onClick={onClick}>
			{output}
		</button>
	);
}

export default AppButton;
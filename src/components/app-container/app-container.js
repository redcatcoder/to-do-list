import React, {Component} from 'react';

import './app-container.css';

import ListInfo from "../list-info/";
import ListInteractive from "../list-interactive";
import List from "../list";
import ListItemAddForm from "../list-item-add-form";

export default class AppContainer extends Component {

	createItem = ( label ) => {
		return {
			label,
			done: false,
			important: false,
			hidden: false
		}
	};

	deleteItem = (id) => {

		this.setState( ({todoData}) => {
			const newTodoData = todoData.filter( (value, key) => {
				if ( key !== id ) return value;

			} )

			return {
				todoData: newTodoData
			};
		} );
	};

	addNewItem = (text) => {
		this.setState( ({todoData}) => {
			const newItem = this.createItem( text );

			const newTodoData = [...todoData, newItem];

			return {
				todoData: newTodoData
			};
		} );
	};

	onToggleImportant = (id) => {
		this.setState( ( {todoData} ) => {
			return {
				todoData: this.toggleProperty( todoData, id, 'important' )
			};
		} )
	};

	onToggleDone = (id) => {
		this.setState( ( {todoData} ) => {
			return {
				todoData: this.toggleProperty( todoData, id, 'done' )
			};
		} );
	};

	toggleProperty = ( data, id, propertyName ) => {
		const newItem = { ...data[id], [propertyName]: !data[id][propertyName]};

		return data.map( ( element, key ) => {
			if ( key === id)
				return newItem;

			return element;
		} )
	};

	onSearchItem = ( searchPhrase ) => {
		this.setState( ({todoData}) => {
			const newTodoData = todoData.map( element => {
				if ( !element.label.includes( searchPhrase ) )
					return {...element, hidden: true}

				return {...element, hidden: false}
			} )

			return {
				todoData: newTodoData
			}
		} )
	};

	filterItems = ( attribute, value ) => {
		return this.state.todoData.filter( element => element[attribute] === value )
	}

	filter = (status) => {
		switch (status) {
			case 'done': {
				return this.filterItems('done', true)
			}
			case 'active': {
				return this.filterItems('done', false)
			}
			default: {
				return this.filterItems('*')
			}
		}
	}

	onChangeFilter = ( filter ) => {
		this.setState( {
			filter: filter
		} )
	}

	state = {
		todoData: [
			this.createItem("Drink coffee"),
			this.createItem("Make awesome app"),
			this.createItem("Have a lunch")
		],
		doneItems: 0,
		leftItems: 0,
		filter: 'all'
	}

	render() {
		const { todoData, filter } = this.state;

		const visibleItems = this.filter( filter )

		const doneCount = todoData.filter( element => element.done ).length;
		const leftCount = todoData.length - doneCount;

		return (
			<>
				<ListInfo done={ doneCount } left={ leftCount }/>
				<ListInteractive onTypeSearch={ this.onSearchItem } onChangeFilter={this.onChangeFilter} />
				<List
					todos={visibleItems}
					onDeleted={ this.deleteItem }
					onToggleImportant={ this.onToggleImportant }
					onToggleDone={ this.onToggleDone }
				/>
				<ListItemAddForm onItemAdded={ this.addNewItem }/>
			</>
		);
	}

}

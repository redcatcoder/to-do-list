import React, {Component} from "react";
import AppButton from "../app-button";

import './list-filter.css';

export default class ListFilter extends Component {
	activeClassName = 'btn-active';

	onSelectFilter = ( event ) => {
		let target = event.target;

		this.props.onChangeFilter( target.dataset.status );
		document.querySelector(`.${this.activeClassName}`).classList.remove(this.activeClassName);
		target.classList.add(this.activeClassName);
	}

	render() {

		const {className} = this.props;

		return (
			<section className={className}>
				<AppButton text={'All'} data={{status: 'all'}} selectors={[this.activeClassName]} onClick={this.onSelectFilter}/>
				<AppButton text={'Active'} data={{status: 'active'}} onClick={this.onSelectFilter}/>
				<AppButton text={'Done'} data={{status: 'done'}} onClick={this.onSelectFilter}/>
			</section>
		);
	}
}

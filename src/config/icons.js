import IconTrash from "../icons/icon-trash.svg";
import IconImportant from "../icons/icon-important.svg";

const Icons = {
	trash: IconTrash,
	important: IconImportant,
}

export default Icons;
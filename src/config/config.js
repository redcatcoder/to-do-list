import Icons from "./icons";

const Config = {
	icons: {
		trash: Icons.trash,
		important: Icons.important,
	}
}


export default Config;
